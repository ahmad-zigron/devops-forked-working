#!/bin/bash

# jenkins deployment script for production deployment of PDNS ui project.

# Get the latest codebase of automation scripts

#cd $WORKSPACE/automationscripts
#git fetch
#git reset --hard origin/master

# checkout release code of pdnsui

function tryexec() {
     cmd=$1
     for x in {{1..1}}; do
       eval $cmd
       if [[ $? -eq 0 ]]; then
         return
       fi
    done
    echo "Failed to execute $cmd"
    exit 1
}

# this script run on prod server. repo is at this fixed path
#cd $WORKSPACE/pdnsui
#tryexec git fetch
#tryexec git checkout $TAG

# copy code files to /usr/share/nginx/html of production server

tryexec ssh ubuntu@13.58.102.223 rm -rf /usr/share/nginx/html/*
tryexec scp * ubuntu@13.58.102.223:/usr/share/nginx/html/

# build latest ui code

tryexec ssh ubuntu@13.58.102.223 cd /usr/share/nginx/html
tryexec ssh ubuntu@13.58.102.223 sudo npm install
tryexec ssh ubuntu@13.58.102.223 bower install
tryexec ssh ubuntu@13.58.102.223 sudo gulp build
