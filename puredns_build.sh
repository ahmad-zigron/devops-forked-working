#!/bin/bash

function tryexec() {
  cmd=$1
  for x in {{1..5}}; do
    #echo $x
    eval $cmd
    if [[ $? -eq 0 ]]; then
      return
    fi
  done
  echo "Failed to execute $cmd"
  exit 1
}
echo " checking out Master branch of automationscripts....."
tryexec "cd /etc/pdns-stage/automationscripts"
tryexec "git stash"
tryexec "git fetch"
tryexec "git checkout master"
tryexec "git pull"
echo " running build.sh script for MASTER branch!!!"
tryexec "./stage-build.sh"