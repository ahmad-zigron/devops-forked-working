#!/bin/bash
function tryexec() {
  cmd=$1
  for x in {{1..3}}; do
    eval $cmd
    if [[ $? -eq 0 ]]; then
      return
    fi
  done
  echo "Failed to execute $cmd"
  exit 1
}
tryexec "cd /etc/pdns-stage/automationscripts/ansible"
echo "Deploying -- Develop ---on $HOSTS"
tryexec "ansible-playbook --vault-password-file pass setup_purevpn_stag.yml"